import styles from './Header.module.css'

const Header = () => {

    return (
        <div className={styles.head_wrap}>
            <div className={styles.head_logo}>
                <span> Redhead Music </span>
            </div>

            <ul className={styles.head_controls_nav}>
                <li>
                    <a> Главное </a>
                    <svg xmlns="http://www.w3.org/2000/svg" className={styles.head_red_dot}>
                        <circle cx="5" cy="5" r="5" fill="#07c" />
                    </svg>
                </li>
                <li>
                    <a> Подкаcты и книги </a>
                </li>
                <li>
                    <a> Детям </a>
                </li>
                <li>
                    <a> Потоки </a>
                </li>
                <li>
                    <a> Коллекция </a>
                </li>
            </ul>

            <div className={styles.head_input_wrapper}>
                <input
                    type="search"
                    className={styles.head_input}
                />
                <img src='/loop.png' className={styles.lupa}></img>
            </div>
        </div>
    )
}

export default Header
